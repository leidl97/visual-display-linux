package web.service;

public interface HDFSIndexService {
	void create() throws Exception;
	Object getHDFSIndex();
}

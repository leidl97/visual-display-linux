package web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import web.entity.ComputerIndex;
@Mapper
public interface ComputerIndexDao {
	void create(ComputerIndex computerIndex);
	ComputerIndex getComputerIndex();
	void deleteComputerIndex();
	int getCount();
	List<ComputerIndex> getAll();
}

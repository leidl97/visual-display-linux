package web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import web.entity.HDFSIndex;
@Mapper
public interface HDFSIndexDao {
	void create(HDFSIndex hdfsIndex);
	HDFSIndex getHDFSIndex();
	List<HDFSIndex> getAll();
}
